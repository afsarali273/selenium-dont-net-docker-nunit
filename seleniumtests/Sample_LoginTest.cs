using System;
using System.Runtime.InteropServices;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace seleniumtests
{
    public class Sample_LoginTest
    {

        IWebDriver driver;

        [SetUp]
        public void startBrowser()

        {

            var options = new ChromeOptions();

                options.AddArgument("--headless");
                options.AddArgument("--no-sandbox");

             driver = new ChromeDriver(options);
          
            //options.AddAdditionalCapability(CapabilityType.PlatformName, "Windows 10", true);
           // options.AddAdditionalCapability("name", TestContext.CurrentContext.Test.Name, true);

           //driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), options.ToCapabilities(),TimeSpan.FromSeconds(600));
        }

        [Test]
        public void test_1()
        {

            driver.Navigate().GoToUrl("https://demosite.executeautomation.com/Login.html");

            driver.FindElement(By.CssSelector("#userName > p:nth-child(1) > input[type=text]")).SendKeys("username");
            driver.FindElement(By.CssSelector("#userName > p:nth-child(2) > input[type=text]")).SendKeys("password");
            driver.FindElement(By.XPath("//*[@id='userName']/p[3]/input")).Click();

            Boolean isDisplayed = driver.FindElement(By.CssSelector("body > h1")).Displayed;
            
            Console.WriteLine("isSelected : " + isDisplayed);

            Assert.IsTrue(isDisplayed);
        }

        [Test]
        public void test_2()
        {

            driver.Navigate().GoToUrl("https://demosite.executeautomation.com/Login.html");

            driver.FindElement(By.CssSelector("#userName > p:nth-child(1) > input[type=text]")).SendKeys("username");
            driver.FindElement(By.CssSelector("#userName > p:nth-child(2) > input[type=text]")).SendKeys("password");
            driver.FindElement(By.XPath("//*[@id='userName']/p[3]/input")).Click();

            Boolean isDisplayed = driver.FindElement(By.CssSelector("body > h1")).Displayed;

            Console.WriteLine("isSelected : " + isDisplayed);
            Console.WriteLine("Title : " + driver.Title);
            Console.WriteLine("Pagesource : " + driver.PageSource);

            Assert.IsTrue(isDisplayed);
        }

        [TearDown]
        public void closeBrowser()
        {
            driver.Quit();
        }



    }
}
