# .NET Core Selenium Tests with NUnit + Docker + Gitlab CI

A simple test implemented in .NET Core 3.1 that uses Selenium WebDriver with multiple browsers.

GitLab Continious Integration Pipeline is being set up , Automatic build will trigger when we will push some code changes to the branch.Altimately it will build the .Net Core and  Selenium with Nunit framework then Dockerize into a docker conatiner.

Docker container will be having :
1.Chrome Browser installed (Manual installed each time) [We can avoid this by using selenium grid docker image]
2.dotnet/sdk:3.1
3.Selenium 3.141
4.Nunit
5.TRX reports (XML report) [TODO- Automated trx to html report generation]

```bash
git clone https://gitlab.com/afsarali273/selenium-dont-net-docker-nunit.git
cd selenium-dont-net-docker-nunit
dotnet clean
dotnet build
dotnet test
```

```bash
dotnet test --logger trx  // for test report

dotnet test --logger "console;verbosity=detailed"
```
**To Run from Docker file :**

Make sure you have docker installed in your machine
```bash
cd seleniumtests

docker build -t selenium-automation-image .

docker run -it selenium-automation-image:latest
```

![CI_PIPELINE](https://gitlab.com/afsarali273/selenium-dont-net-docker-nunit/-/blob/master/image/CI_PIPELINE.png)


